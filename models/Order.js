//Capstone 2 Order Model
const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
	totalAmount : {
		type: Number,
	},
	purchasedOn : {
		type: Date,
		default: new Date()
	},
	customerId: {
		type: String,
		required: [true, "Id is required"]
	},
	customerName: {
	type: String,
	required: [true, "Name is required"]
	},
	orderedProducts : [{
		productId : {
			type: String,
			required: [true, "Id is required"]
		},
		productName : {
			type: String,
			required: [true, "Name is required"]
		},
		productPrice: {
			type: Number,
			required: [true, "Price is required"]
		},
		quantity: {
		type: Number,
		required: [true, "Price is required"]
		}
	}
	]
});

module.exports = mongoose.model("Order", orderSchema);