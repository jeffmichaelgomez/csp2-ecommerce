const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require("../auth");

//add to cart
router.post("/users/checkout",auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.addProduct(userData,req.body).then(resultFromController => res.send(resultFromController));
})

//get all orders
router.get("/users/orders",auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.getAllOrders(userData).then(resultFromController => res.send(resultFromController));
})

//get myOrder
router.get("/users/myOrders",auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.getMyOrders(userData).then(resultFromController => res.send(resultFromController));
})

module.exports = router;