const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");

module.exports.addProduct = async (userData,product) => {
	if(userData.isAdmin == false) {
		let userId = await User.findOne({_id: userData.id}).then(result => {
			console.log(result.id);
			return result.id;
		})
		let userName = await User.findOne({_id: userData.id}).then(result => {
		console.log(result.name);
		return result.name;
		})
		let productId = await Product.findOne({_id: product.id}).then(result => {
			console.log(result.id);
			return result.id;
		})
		let productPrice = await Product.findOne({_id: product.id}).then(result => {
			console.log(result.price);
			return result.price;
		})
		let productName = await Product.findOne({_id: product.id}).then(result => {
		console.log(result.name);
		return result.name;
		})
		function stockUpdate(){Product.findById(productId).then(result => {
		result.stock = result.stock-1;
		result.save();
		}) }

		return await Order.findOne({customer: userId}).then(result => {
			if (result == null){
				let newOrder = new Order({
					customerId : userId,
					customerName : userName,
					orderedProducts : ({productId:productId,productName:productName, productPrice:productPrice,quantity:1}),
					totalAmount: productPrice
				})
				return newOrder.save().then((user,error) => {
					if (error){
						return false;
					}
					else {
						stockUpdate();
						return `Order successfully added`;
					}
				})
			}
			else{
				return Order.findOne({customer: userId}).then(order => {
					order.orderedProducts.push({productId:productId,productName:productName, productPrice:productPrice,quantity:1});
					order.totalAmount = order.orderedProducts.reduce((a, b) => a + b.productPrice, 0);
					order.save();
					stockUpdate();
					return `Order successfully added`; 
				})
			}
		})
	}
	else{
		return "Kindly login a User Account"
	}
}


module.exports.getAllOrders = async (userData) => {
	if(userData.isAdmin) {
		return Order.find({}).then(result => {
			return result;
		})
	}
	else{
		return "You have no access"
	}
}

module.exports.getMyOrders = async (userData) => {
	if(userData.isAdmin == false) {
		return Order.findOne({customerId:userData.id}).then(result => {
			if (result == null){
				return "Your cart is currently empty"	
			}
			else{
				return result;
			}
		})
	}
	else{
		return "Kindly login a User Account"
	}
}
