const Product = require("../models/Product");
// npm install bcrypt
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.getAllProduct = async (userData,reqBody) => {
	if(userData.isAdmin) {
		return Product.find({}).then(result => {
			return result;
		})
	}
	else{
		return "You have no access"
	}
}


module.exports.getActiveProduct = (reqBody) => {
		return Product.find({isActive: true}).then(result => {	
			return result;
		})
}

module.exports.getProduct = (productId,reqBody) => {
		return Product.findById(productId).then((result) =>{
			if(result == null){
				return "Product does not exist."
			}
			if(result.isActive){
				return result;	
			}
			else{
				return `${reqBody.name} is out of stock.`		
			}
		})
	}

module.exports.registerProduct = async (userData,reqBody) => {
	if(userData.isAdmin) {
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price : reqBody.price,
			stock:reqBody.stock
		})

		return await Product.findOne({name: reqBody.name}).then(result => {
			if(result == null){
				return newProduct.save().then((user,error) => {
					if (error){
						return false;
					}
					else {
						return `${reqBody.name} has been successfully added to product list.`;
					}
				})
			}
			else{
				return "Product already exists.";
			}
		})
	}
	else{
		return "You have no access"
	}
}

module.exports.updateProduct = async (userData,productId,reqBody) => {
	if(userData.isAdmin) {
		return await Product.findById(productId).then((result) =>{
			if(result == null){
				return "Product does not exist."
			}

			else{
				result.name = reqBody.name
				result.description = reqBody.description;
				result.price = reqBody.price;
				result.stock = reqBody.stock;
				return result.save().then((user,error) => {
					if (error){
						return false;
					}
					else {
						return `${result.name} is now Updated`;
					}
				})
			}
		})
	}
	else{
		return "You have no access";
	}
}

module.exports.archiveProduct = async (userData,productId) => {
	if(userData.isAdmin) {
		return await Product.findById(productId).then((result) =>{
			if(result == null){
				return "Product does not exist."
			}

			else{
				result.isActive = false;
				return result.save().then((user,error) => {
					if (error){
						return false;
					}
					else {
						return `${result.name} successfully Updated`;
					}
				})
			}
		})
	}
	else{
		return "You have no access";
	}
}

module.exports.restockProduct = async (userData,productId) => {
	if(userData.isAdmin) {
		return await Product.findById(productId).then((result) =>{
			if(result == null){
				return "Product does not exist."
			}

			else{
				result.isActive = true;
				return result.save().then((user,error) => {
					if (error){
						return false;
					}
					else {
						return `${result.name} successfully Updated`;
					}
				})
			}
		})
	}
	else{
		return "You have no access";
	}
}