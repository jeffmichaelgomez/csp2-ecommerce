const User = require("../models/User");
// npm install bcrypt
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.getUsers = async (userData,reqBody) => {
	if(userData.isAdmin) {
		return User.find({}).then(result => {	
			return result;
		})
	}
	else{
		return "You have no access"
	}
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		name: reqBody.name,
		address: reqBody.address,
		email : reqBody.email,
 		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return newUser.save().then((user,error) => {
				if (error){
					return false;
				}
				else {
					return `Congratulations ${reqBody.name}, your account has been successfully created.`;
				}
			})
		}
		else{
			return "Email already exist.";
		}
	})
} 

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return "Email does not exist";
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
				}
			else{
				return "Password is incorrrect";
			}
		}	
	})
}

module.exports.setAdmin = async (userData,userId,reqBody) => {
	if(userData.isAdmin) {
		return await User.findById(userId).then((result,error) =>{
			if(error){
				return "UserID does not exist."
			}
			if(reqBody.isAdmin == false){
				result.isAdmin = false;
				return result.save().then((user,error) => {
					if (error){
						return false;
					}
					else {
						return `${result.name}'s authorization successfully updated.`;
					}
				})
			}
			else{
				result.isAdmin = true;
				return result.save().then((user,error) => {
					if (error){
						return false;
					}
					else {
						return `${result.name}'s authorization successfully updated.`;
					}
				})
			}
		})
	}
	else{
		return "You have no access";
	}
}